using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships
{
    class Program
    {
        private static BattleGrid _occupiedArea;
        private static int[] _target;
        private static List<int[]> _previousHits;
        static void Main(string[] args)
        {
            Setup();          
        }

        /// <summary>
        /// Create the game, generate the ships in unique locations in the grid
        /// 
        /// </summary>
        static void Setup()
        {
            Console.WriteLine("Generating a New game");
            //for variations on the game adjust the first two parameters, 1st- # of 5 length battleships
            // 2nd - # of 4 length destroyers.
            _occupiedArea = new BattleGrid(1,2,10);
            StartGame();
            Console.WriteLine("End of the Game, If you would like to play again please hit 1, otherwise hit any key to leave.");
            string keepPlaying = Console.ReadLine();
            if(keepPlaying.Length > 0)
            {
                if (Convert.ToInt32(Console.ReadLine()) == 1)
                {
                    Setup();
                }  
            }
        }

        /// <summary>
        /// Allow for user input to play the game
        /// </summary>
        static void StartGame()
        {
            Console.WriteLine("Lets Start...");
            Console.WriteLine("Please choose a Cooordinate to Attack (A - J) (1 - 10) eg. C6 and hit Enter.");
            Play();
            
        }

        /// <summary>
        /// Gameplay keep shooting until all ships are destroyed
        /// </summary>
        private static void Play()
        {
            _previousHits = new List<int[]>();
            bool gameover = false;
            while (!gameover)
            {
                string entry = Console.ReadLine();
                if (IsValid(entry))
                {
                    if (_occupiedArea.HasBeenHit(_target))
                    {
                        Console.WriteLine("HIT - Hooray, keep firing");
                        _previousHits.Add(_target);
                        CheckShipStatus();
                        ShowPreviousHits();
                    }
                    else
                    {
                        Console.WriteLine("Unfortunately you have missed, please try again.");
                    }
                }
                else
                {
                    Console.WriteLine(entry + ": Is not a valid target, please enter a Letter between A and J followed by a number between 1 and 10");
                }
                if(_occupiedArea._battleShips.Count == 0)
                {
                    Console.WriteLine("All ships destroyed, you WIN!");
                    gameover = true;
                }
            }
        }

        /// <summary>
        /// List all the previous hits to make an educated guess on the next target
        /// </summary>
        private static void ShowPreviousHits()
        {
            Console.WriteLine("Previous Hits...");
            foreach(int[] hit in _previousHits)
            {
                Console.Write("(" + NumberToString(hit[0]) + "" + (hit[1]+ 1) + ") ");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Check status of remaining ships, and remove them if they have been destroyed
        /// </summary>
        private static void CheckShipStatus()
        {
            foreach(Battleship ship in _occupiedArea._battleShips)
            {
                if(ship._length == 0)
                {
                    Console.WriteLine("HIT and Sunk");
                    _occupiedArea._battleShips.Remove(ship);
                    Console.WriteLine("Ships left = " + _occupiedArea._battleShips.Count);
                    break;
                }
                
            }
        }

        /// <summary>
        /// A check to see if coordinates entered by the user are valid
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private static bool IsValid(string entry)
        {
            entry = entry.ToUpper();
            int xCoordinate, yCoordinate;
            if(entry.Length < 2 || entry.Length > 3)
            {
                return false;
            }
            else
            {
                xCoordinate = entry[0] - 65; //ascii offset changing letter to number
                try
                {
                    Console.WriteLine(entry.Length);
                    string numberAsString = string.Empty;
                    if(entry.Length > 2 )
                    {
                        numberAsString += entry[1] + "" + entry[2];
                    }
                    else
                    {
                        numberAsString += entry[1];
                    }

                    yCoordinate = Convert.ToInt32(numberAsString) - 1;
                    Console.WriteLine("X coordinate = " + xCoordinate + ": Y coordinate = " + yCoordinate);
                }
                catch
                {
                    return false;
                }
                if(xCoordinate < 0 || xCoordinate > 9 || yCoordinate < 0 || yCoordinate > 9)
                {
                    Console.WriteLine("Coordinates are out of bounds");
                    return false;
                }
                _target = new int[] { xCoordinate, yCoordinate };
            }

            return true;
        }

        /// <summary>
        /// Changing numeric value to Alphabet character for feedback to the user
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static String NumberToString(int number)
        {
            Char c = (Char)(65  + (number));
            return c.ToString();
        }



    }
}
