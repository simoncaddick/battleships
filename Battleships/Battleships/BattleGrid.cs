﻿using System;
using System.Collections.Generic;

namespace Battleships
{
    internal class BattleGrid
    {
        public int _numberBattleShips { get; set; }
        public int _numberDestroyers { get; set; }
        public int _gridSize { get; set; }
        public List<int[]> _allocatedArea  { get; set; }
        public List<Battleship> _battleShips { get; set; }

        private Random random = new Random();

        /// <summary>
        /// Constructor for the Battlefield / Grid
        /// </summary>
        /// <param name="battleShips"></param>
        /// <param name="destroyers"></param>
        /// <param name="size"></param>
        public BattleGrid(int battleShips, int destroyers, int size)
        {
            _numberBattleShips = battleShips;
            _numberDestroyers = destroyers;
            _gridSize = size;
            _allocatedArea = new List<int[]>();
            _battleShips = new List<Battleship>();


            for(int i = 0; i < _numberBattleShips; i++)
            {
                Battleship battleship = new Battleship(5);
                _battleShips.Add(battleship);
                AllocateSpace(battleship);
            }
            for (int i = 0; i < _numberDestroyers; i++)
            {
                Battleship battleship = new Battleship(4);
                _battleShips.Add(battleship);
                AllocateSpace(battleship);
            }
        }

        /// <summary>
        /// Assign a ship to an area of the map
        /// </summary>
        /// <param name="ship"></param>
        private void AllocateSpace(Battleship ship)
        {
            bool isVertical = IsVertical();
            bool previouslyOccupied = false;
            int coordinateA = new Random().Next(_gridSize - ship._length);
            int coordinateB = new Random().Next(_gridSize);
            List<int[]> shipCoordinates = new List<int[]>();

            if (isVertical)
            {
                for(int i = 0; i < ship._length; i++)
                {
                    int[] coordinate = new int[] { coordinateB, coordinateA + i };
                    previouslyOccupied = IsPreviouslyAllocated(coordinate);
                    if (previouslyOccupied)
                    {
                        break;
                    }
                    else
                    {
                        shipCoordinates.Add(coordinate);
                    }
                }
            }
            else
            {
                for (int i = 0; i < ship._length; i++)
                {
                    int[] coordinate = new int[] { coordinateA + i , coordinateB};
                    previouslyOccupied = IsPreviouslyAllocated(coordinate);
                    if (previouslyOccupied)
                    {
                        break;
                    }
                    else
                    {
                        shipCoordinates.Add(coordinate);
                    }
                }

            }
            if (previouslyOccupied)
            {
                AllocateSpace(ship);
            }
            else
            {
                ship._shipCoordinates = shipCoordinates;
                foreach(int[] coordinate in shipCoordinates)
                {
                    _allocatedArea.Add(coordinate);
                    // Uncomment to debug- This will display all the c  oordinates of the ships to make testing easier
                    //Console.WriteLine(coordinate[0] + ", " + coordinate[1]); 
                }
            }
            
        }

        /// <summary>
        /// Ensure that the area of the map has not previously been used
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public bool IsPreviouslyAllocated(int[] coordinate)
        {
            foreach(int[] position in _allocatedArea)
            {
                if(position[0] == coordinate[0] && position[1] == coordinate[1])
                {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Check if the ship has been hit
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public bool HasBeenHit(int[] coordinate)
        {
            foreach (Battleship ship in _battleShips)
            {
                foreach (int[] position in ship._shipCoordinates)
                {
                    if (position[0] == coordinate[0] && position[1] == coordinate[1])
                    {
                        ship._shipCoordinates.Remove(position);
                        ship._length--;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Randomly decide whether the ship will be veritcal or horizontal
        /// </summary>
        /// <returns></returns>
        private bool IsVertical()
        {
            int rnd = random.Next(2);
            return rnd == 0;

        }
    }
}