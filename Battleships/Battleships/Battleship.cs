﻿using System.Collections.Generic;

namespace Battleships
{
    internal class Battleship
    {
        public int _length { get; set; }
        public List<int[]> _shipCoordinates { get; set; }
        public Battleship(int length)
        {
            _length = length;
        }
    }
}