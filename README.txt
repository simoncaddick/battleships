README: Battleships

This version of Battleships is a one sided game of the User playing against the Computer. 
The computer will place 3 ships; 1 Battleship Length of 5, and 2 Destroyers Lenght of 4 on a 10 x 10 grid. 
These values may be changed in the solution to add as many ships of two types as the user 
would like in a grid of as many units as the user would like. (some altering will be needed 
to accomodate rows greater than 10 although collumns should be good to 26.)

Once the game has set itself up the user takes shots, naming a grid square with an alphabet 
character and a numeric value, the game design is targeted for A1 - J10.

Have fun, enjoy, this is not perfect.